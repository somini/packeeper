# packeeper

`pacman` state tracker, ready to be integrated on `etckeeper`.

Based on [this Arch Wiki article](https://wiki.archlinux.org/title/Pacman/Tips_and_tricks#List_of_installed_packages).

## Usage

Install the AUR package. Nothing else should be needed to track the pacman
state at `/etc/pacman.d/state`, fully integrated with `etckeeper`, producing
clean diffs.

Runs `packeeper -q` after all package manager operations, to recalculate the state.

### Manual Setup

Take `contrib/35-packeeper` and install it on:
- `/etc/etckeeper/pre-install.d/45-packeeper`
- `/etc/etckeeper/post-install.d/45-packeeper`

This will update the packeeper state before and after installation changes.

## See Also

Other solutions to this problem:

- [bacpac](https://aur.archlinux.org/packages/bacpac/)
- [packup](https://aur.archlinux.org/packages/packup/)
- [pacmanity](https://aur.archlinux.org/packages/pacmanity/)
- [pug](https://aur.archlinux.org/packages/pug/)
